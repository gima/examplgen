## examplgen

Insert example code into a document using:

* Template with go's "[text/template](http://golang.org/pkg/text/template/)" markup
* Golang source code

*For example: insert code examples to your project's readme.*


#### Install

```shell
go get "github.com/gima/examplgen"
```


## Template and Code

Contents of `README.md.tpl`:
>     ## Project
>     
>     Description.
>     
>     #### Example Code
>     ```go
>     {{.CodeExample1}}
>     ```
>     
>     End of Readme.

Contents of `v1/tests/introduction_test.go`:
> ```go
> package example_test
> 
> import "fmt"
> 
> func CodeExample1() {
> 	v := 1 + 2
> 
> 	fmt.Printf("1 + 2 = %d", v)
> }
> ```



## Run

```
examplgen -template README.md.tpl -code v1/tests/introduction_test.go > README.md
```

Output:
>     ## Project
>     
>     Description.
>     
>     #### Example Code
>     ```go
>     v := 1 + 2
>     
>     fmt.Printf("1 + 2 = %d", v)
>     ```
>     
>     End of Readme.


## License

http://unlicense.org  
Authoritative: UNLICENSE.txt  
Mention of origin would be appreciated.

*go, golang, go/parser, template, text/template, code, example, generator, generate, readme*
