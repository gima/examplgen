package lib

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"strings"
	templatepkg "text/template"
)

func Blurt(template, code string, out io.Writer) error {
	// Note: text/template takes string and doesn't appear to support io.Reader,
	// while go/parser does. for uniformity, both parameters are strings.

	var err error
	var tpl *templatepkg.Template
	var fset *token.FileSet
	var f *ast.File
	m := map[string]string{}

	for i := 1; err == nil; i++ {
		switch i {
		case 1:
			// parse template
			tpl, err = templatepkg.New("").Parse(template)

		case 2:
			// parse code
			fset = token.NewFileSet()
			f, err = parser.ParseFile(fset, "input_file.go", []byte(code), 0)

		case 3:
			// prepare map key=funcname value=func body
			for _, n := range f.Decls {
				if fdecl, ok := n.(*ast.FuncDecl); ok {
					m[fdecl.Name.Name] = code[int(fdecl.Body.Lbrace) : int(fdecl.Body.Rbrace)-1]
				}
			}

		case 4:
			// cleanup function bodies
			for k, v := range m {
				arr := strings.Split(v, "\n")

				rtrim(&arr)
				removePrePostEmptyLines(&arr)
				commonDeIndent(&arr)

				m[k] = strings.Join(arr, "\n")
			}

		case 5:
			// use template
			err = tpl.Execute(out, m)

		default:
			return nil
		}
	}

	return fmt.Errorf("Examplgen failed: %+v", err)
}

// remove \r and \n from right of lines
func rtrim(arr *[]string) {
	for i, s := range *arr {
		(*arr)[i] = strings.TrimRight(s, "\r\n")
	}
}

// remove empty lines from beginning and end of string
func removePrePostEmptyLines(arr *[]string) {
	var s string

	var cutfrom int
	for cutfrom, s = range *arr {
		s := strings.TrimSpace(s)
		if s != "" {
			break
		}
	}

	var cutto int
	for cutto = len(*arr) - 1; cutfrom <= cutto; cutto-- {
		s := strings.TrimSpace((*arr)[cutto])
		if s != "" {
			break
		}
	}
	*arr = (*arr)[cutfrom : cutto+1]
}

// remove least common denomnominator amount of empty tabs from beginning of lines
func commonDeIndent(arr *[]string) {
	var minpretabs int

	for _, s := range *arr {
		li := -1
		for i, r := range s {
			if r != '\t' {
				break
			}
			li = i
		}
		if li != -1 && li < minpretabs {
			minpretabs = li
		}
	}

	for i, s := range *arr {
		if minpretabs == -1 || len(s) == 0 {
			continue
		}
		(*arr)[i] = s[minpretabs+1:]
	}
}
