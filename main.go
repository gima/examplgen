package main

import (
	"flag"
	"fmt"
	"github.com/gima/examplgen/lib"
	"io/ioutil"
	"os"
	"path/filepath"
)

var (
	templatefile = flag.String("template", "", "(file) Template to insert code examples into")
	codefile     = flag.String("code", "", "(file) Code to insert into template")
)

func main() {
	flag.Parse()

	if flag.NFlag() != 2 {
		fmt.Println("Usage:", filepath.Base(os.Args[0]), "[parameters]")
		fmt.Println("  Generate output from template and golang code.")
		fmt.Println("Parameters:")
		flag.PrintDefaults()
		os.Exit(1)
	}

	template := read(*templatefile)
	code := read(*codefile)

	err := lib.Blurt(template, code, os.Stdout)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func read(path string) string {
	ba, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("Read failed:", err)
		os.Exit(1)
	}
	return string(ba)
}
