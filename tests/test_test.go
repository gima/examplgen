package recodex_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/gima/examplgen/lib"
	"github.com/stretchr/testify/require"
)

var template = `# Example 1
{{.CodeExample1}}
# Example 2
{{.CodeExample2}}
End of template.`

func CodeExample1() {
	fmt.Println("Hello World")
}

func CodeExample2() {
	if true {
		fmt.Println("Hello World")
	}
}

var expectedout = `# Example 1
fmt.Println("Hello World")
# Example 2
if true {
	fmt.Println("Hello World")
}
End of template.`

func TestTest(t *testing.T) {
	tplr, err := os.Open("test_test.go")
	require.NoError(t, err)

	tplba, err := ioutil.ReadAll(tplr)
	require.NoError(t, err)

	out := new(bytes.Buffer)

	err = lib.Blurt(template, string(tplba), out)
	require.NoError(t, err)

	require.Equal(t, expectedout, out.String())
}
